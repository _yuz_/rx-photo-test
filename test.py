# models.py

class CompanyType(models.Model):
    type = models.CharField(max_length=100, unique=True)


class Company(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    type = models.ForeignKey('CompanyType', null=True, related_name='companies',
                             on_delete=models.SET_NULL)
    manager = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='managed_companies',
                                on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    headquarter = models.ForeignKey('Office', null=True, on_delete=models.SET_NULL)


class Office(models.Model):
    company = models.ForeignKey('Company', blank=False, null=False, related_name='offices', on_delete=models.CASCADE)

    street = models.CharField(max_length=255, blank=True)
    postal_code = models.CharField(max_length=30, blank=True)
    city = models.CharField(max_length=255, blank=True)

    rent = models.PositiveIntegerField(default=0)

    class Meta:
        index_together = [
            ('street', 'postal_code', 'city'),
        ]

    def __str__(self):
        return f'{self.postal_code} {self.city} {self.street}'.replace(' ', '') or f'<ofice id: {self.id}>'


# serializers.py
class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ('id', 'street', 'postal_code', 'city')


class CompanySerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255)
    street = serializers.CharField(source='headquarter.street', max_length=255, read_only=True)
    postal_code = serializers.CharField(source='headquarter.postal_code', max_length=30, read_only=True)
    city = serializers.CharField(source='headquarter.city', max_length=255, read_only=True)
    all_offices_rent = serializers.SerializerMethodField()

    class Meta:
        model = Office
        fields = ('id', 'name', 'street', 'postal_code', 'city', 'type', 'all_offices_rent')
        extra_kwargs = {
            'type': {'write_only': True},
        }

    def get_all_offices_rent(self, obj):
        return getattr(obj, 'all_offices_rent', obj.offices.aggregate(sum=Sum('rent'))['sum'])


# views.py

class CompanyListViewSet(viewsets.mixins.RetrieveModelMixin,
                         viewsets.mixins.UpdateModelMixin,
                         viewsets.mixins.ListModelMixin,
                         viewsets.GenericViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.select_related('headquarter').annotate(all_offices_rent=Sum('offices__rent'))

    @action(detail=True, methods=('get',), url_path='offices', serializers=OfficeSerializer)
    def offices(self, *args, **kwargs):
        serializer = self.get_serializer(self.get_object().offices.all(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=('post',), url_path='set-headquarter', serializers=serializers.Serializer)
    def set_headquarter(self, *args, **kwargs):
        try:
            company = self.get_object()
            headquarter = Office.objects.get(id=self.request.data['headquarter'], company=company)
            company.headquarter = headquarter
            company.save(update_fields=('headquarter',))
        except (ValueError, ObjectDoesNotExist, KeyError):
            raise ValidationError('wrong paarams')

        return Response({'success': True})


# urls.py

router.register(r'companies', CompanyListViewSet, base_name='company')


# admin.py

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = 'name', 'headquarter', 'all_offices_rent_display', 'total_offices_display',

    def get_queryset(self, request):
        return (super().get_queryset(request=request)
                .annotate(_all_offices_rent=Sum('offices__rent'), total_offices=Count('id')))

    def all_offices_rent_display(self, obj):
        return obj._all_offices_rent

    all_offices_rent_display.short_description = 'total monthly rent'

    def total_offices_display(self, obj):
        return obj.total_offices

    total_offices_display.short_description = 'total number of offices'


# part of migration

def add_default_company_types(apps, schema_editor):
    _CompanyType = apps.get_model('some_app', 'CompanyType')

    (_CompanyType.objects
     .using(schema_editor.connection.alias)
     .bulk_create([_CompanyType(type=item) for item in ('Client', 'Contractor', 'Supplier')]))


# ~~~~~~~~~~~~~~~~

operations = [
        migrations.RunPython(add_default_company_types, migrations.RunPython.noop),
    ]